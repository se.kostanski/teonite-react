export const getSortedData = (state) => {

    //Concat users data into single object
    let concatUsers = []; 

        [...state].map((user) => {
            let id = 1;
            for(let key in user) {
                for(let word in user[key]) {
                    concatUsers = [...concatUsers, {id: id, word: word, stats: user[key][word]}];
                    id++;
                 }
            }
          })

        //Merge duplicated objects
        const result = concatUsers.reduce((result, item) => {
            const existing = result.find(x => x.word === item.word);
            
            if (existing) {
               existing.stats += item.stats;
            } else {
              result.push(item);
            }
            
            return result;
          }, []);
        
        //Sort object stats
        const sortedData = result.sort((a, b) => (a.stats > b.stats) ? 1 : ((b.stats > a.stats) ? 0 : -1));
    
    return sortedData;
  };